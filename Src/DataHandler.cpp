#include "../Inc/DataHandler.h"	// Used for 
#include <iostream>							// Used for 
#include <bitset>								// Used for 
#include <assert.h>							// Used for 

using namespace BP::Data;

DataHandler::DataHandler(const size_t arraySize)
	: m_data(new uint32_t[arraySize])
	, m_arraySize(arraySize) {

	clearData();
}

DataHandler::~DataHandler() {
	delete[] m_data;
}

bool DataHandler::addData(const uint32_t data, const uint8_t location) {
	uint32_t temp {static_cast<uint32_t>(location) * 9};
	uint32_t counter{0};
	bool splitFlag{false};

	// Check which element the data should be added to
	while (temp >= 32) {
		temp -= 32;
		++counter;
	}

	// Check if counter is still within the range of the array
	if (counter >= m_arraySize)
		return false;

	// Check if the data should be split over two elements
	if (temp > 23) {
		temp = 32 - temp;
		splitFlag = true;
	}

	// Check if counter with split is still within the range of the array
	if (splitFlag && counter + 1 >= m_arraySize)
		return false;

	if (!splitFlag) {
		uint32_t clearData{0xFFFFFE00};
		uint32_t clearDataMask{0xFFFFFFFF};
		clearData <<= temp;
		clearDataMask >>= 32 - temp;
		clearData |= clearDataMask;
		m_data[counter] &= clearData;
		m_data[counter] |= data << temp;
	} else {
		uint32_t clearData1{0xFFFFFFFF};
		uint32_t clearData2{0xFFFFFFFF};
		clearData1 >>= (temp);
		clearData2 <<= (9 - temp);
		m_data[counter] &= clearData1;
		m_data[counter + 1] &= clearData2;
		m_data[counter] |= data << (32 - temp);
		m_data[counter + 1] |= data >> (temp);
	}

	return true;
}

bool DataHandler::addStringData(const std::string &inputData) {
	// ensure there is enough data in the string to fill the entire array
	if (inputData.length() < (m_arraySize * 4))
		return false;

	for (size_t index{0}; index < m_arraySize; ++index) {
		const auto dataAsArray = reinterpret_cast<const uint32_t *>(inputData.data())[index];
		m_data[index] = dataAsArray;
	}

	return true;
}

uint16_t DataHandler::getData(const uint8_t location) const {
	uint32_t temp{static_cast<uint32_t>(location) * 9};
	uint32_t counter{0};
	bool splitFlag{false};

	while (temp >= 32) {
		temp -= 32;
		++counter;
	}

	if (temp > 23) {
		temp = 32 - temp;
		splitFlag = true;
	}

	if (!splitFlag) {
		uint32_t setData{0x01FF};
		return static_cast<uint16_t>((m_data[counter] >> temp) & setData);
	} else {
		uint32_t setData{0x01FF};
		return static_cast<uint16_t>((m_data[counter] >>(32 - temp) | m_data[counter + 1] << temp) & setData);
	}
	return 0;
}

std::string DataHandler::getDataAsString() const {
	std::string dataString{};
	for (size_t arrayIndex{0}; arrayIndex < m_arraySize; ++arrayIndex) {
		for (size_t dataIndex{0}; dataIndex < 4; ++dataIndex)
			dataString += static_cast<char>(m_data[arrayIndex] >>(dataIndex * 8));
	}
	return dataString;
}

const uint32_t *DataHandler::getDataArr() const {
	return m_data;
}

void DataHandler::clearData() {
	for (size_t index{0}; index < m_arraySize; ++index)
		m_data[index] = 0;
}

void DataHandler::invertData() {
	for (size_t index{0}; index < m_arraySize; ++index)
		m_data[index] = ~m_data[index];
}

void DataHandler::encodedecodeData() {
	for (size_t index{0}; index < m_arraySize; ++index)
		m_data[index] ^= 0x55555555U;
}