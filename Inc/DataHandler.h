#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include <cinttypes>
#include <string>

namespace BP::Data {
	/// @brief Save 9 bit data inside 32 bit array
	class DataHandler {
	public:
		DataHandler(const size_t arraySize);
		~DataHandler();

		/// @brief addData adds data to the data array
		/// @param data a 9bit data 0-360 degrees
		/// @param location where the data is stored in the array 32bits 6x total of 21 location options
		/// @return true if data was added
		/// @return false if data was not added
		bool addData(const uint32_t data, const uint8_t location);

		/// @brief convert full string data to array
		/// @param inputData a string containing the corresponding bits
		/// @return true if data was added
		/// @return false if data was not added
		bool addStringData(const std::string &inputData);

		/// @brief getData gets the data from the data array
		/// @param location the location of the data that is requisted 32bits 6x total of 21 location options
		/// @return The data in the given location 0 - 360 degrees
		uint16_t getData(const uint8_t location) const;

		/// @brief getDataAsString gets the data as one continues string
		/// @return data as string
		std::string getDataAsString() const;

		/// @brief getDataArr get the data in array pointer form
		/// @return The data in array pointer form
		const uint32_t *getDataArr() const;

		/// @brief Clears all the data inside the array
		void clearData();

		/// @brief Inverts the data from 0 to 1 and 1 to 0
		void invertData();

		/// @brief Simple encode/decode of the data
		void encodedecodeData();

		inline size_t getArraySize() const {
			return m_arraySize;
		}

	private:
		uint32_t *m_data;		///< Array for storing the data
		size_t m_arraySize;	///< Size of the array
	};
};

#endif // DATAHANDLER_H
