#include <catch2/catch.hpp>

#include <iostream>
#include "../Inc/DataHandler.h"

#include <bitset>

using namespace BP::Data;

constexpr size_t totalAngles{24};
constexpr size_t totalStrings{28};
constexpr size_t arraySize{7};

// *INDENT-OFF*
/// Angles are between 0 and 360 degrees
constexpr uint32_t anglesTestOne[totalAngles] {
  1, 1, 1, // Pink
  1, 1, 1, // Ring
  1, 1, 1, // Middle
  1, 1, 1, // Index
  1, 1, 1, // Thumb
  1, 1, 1, // Hand
  1, 1, 1, // Lower Arm
  1, 1, 1  // Upper Arm
}; 

/// Angles Encoded for test One
constexpr uint32_t encodedAnglesTestOne[arraySize] {
  0b00001000000001000000001000000001,
  0b10000000010000000010000000010000,
  0b00000100000000100000000100000000,
  0b01000000001000000001000000001000,
  0b00000010000000010000000010000000,
  0b00100000000100000000100000000100,
  0b00000000000000001000000001000000
};

constexpr uint32_t anglesTestTwo[totalAngles] {
  356, 200, 123, // Pink
  166,  88,  85, // Ring
  231,  78, 111, // Middle
  341, 231, 121, // Index
  123, 134, 115, // Thumb
  311, 211, 312, // Hand
  213, 214, 215, // Lower Arm
  286,  24,   1  // Upper Arm
}; 

constexpr uint32_t encodedAnglesTestTwo[arraySize] {
  0b00110001111011011001000101100100,
  0b00111001110010101010010110000101,
  0b10011110101010100110111100100111,
  0b11010000110001111011001111001011,
  0b01110000110100111001101110011100,
  0b11001101011101101011001101010110,
  0b00000000000000001000011000100011
};

constexpr uint8_t stringEncodedTestThree[totalStrings] {
  0b01100100, 0b10010001, 0b11101101, 0b00110001,
  0b10000101, 0b10100101, 0b11001010, 0b00111001,
  0b00100111, 0b01101111, 0b10101010, 0b10011110,
  0b11001011, 0b10110011, 0b11000111, 0b11010000,
  0b10011100, 0b10011011, 0b11010011, 0b01110000,
  0b01010110, 0b10110011, 0b01110110, 0b11001101,
  0b00100011, 0b10000110, 0b00000000, 0b00000000
}; 

/// Inverted Angles Encoded from test One
constexpr uint32_t encodedAnglesTestOneInverted[arraySize] {
  0b11110111111110111111110111111110,
  0b01111111101111111101111111101111,
  0b11111011111111011111111011111111,
  0b10111111110111111110111111110111,
  0b11111101111111101111111101111111,
  0b11011111111011111111011111111011,
  0b11111111111111110111111110111111
};

// *INDENT-ON*

TEST_CASE("Encode data", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);

	for (uint8_t angle{0}; angle < totalAngles; ++angle)
		myDataHandler.addData(anglesTestOne[angle], angle);

	for (int index{0}; index < arraySize; ++index) {
		std::bitset<32> y{myDataHandler.getDataArr()[index]};
		REQUIRE(y == encodedAnglesTestOne[index]);
	}
}

/// Check if the data is encoded as expected
TEST_CASE("Decode data", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);

	for (uint8_t angleIndex{0}; angleIndex < totalAngles; ++angleIndex)
		myDataHandler.addData(anglesTestTwo[angleIndex], angleIndex);

	for (uint8_t arrayIndex{0}; arrayIndex < arraySize; ++arrayIndex)
		REQUIRE(std::bitset<32>(myDataHandler.getDataArr()[arrayIndex]).to_ulong() == encodedAnglesTestTwo[arrayIndex]);

	for (uint8_t angleIndex{0}; angleIndex < totalAngles; ++angleIndex) {
		std::bitset<32> angle{myDataHandler.getData(angleIndex)};
		REQUIRE(angle == anglesTestTwo[angleIndex]);
	}
}

/// @brief test when location is larger than availible space
TEST_CASE("Outside valid range", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);
	REQUIRE_FALSE(myDataHandler.addData(0x111, 25));
}

/// @brief test if when a too short of a string is given funciton returns false
TEST_CASE("Convert string data to dataHandler array", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);
	std::string data{};

	for (uint8_t stringIndex{0}; stringIndex < totalStrings; ++stringIndex)
		data += stringEncodedTestThree[stringIndex];
	REQUIRE(myDataHandler.addStringData(data));

	for (uint8_t arrayIndex{0}; arrayIndex < arraySize; ++arrayIndex)
		REQUIRE(std::bitset<32>(myDataHandler.getDataArr()[arrayIndex]).to_ulong() == encodedAnglesTestTwo[arrayIndex]);

	for (uint8_t angleIndex{0}; angleIndex < totalAngles; ++angleIndex) {
		std::bitset<32> angle{myDataHandler.getData(angleIndex)};
		REQUIRE(angle == anglesTestTwo[angleIndex]);
	}
}

/// @brief test if when a too short of a string is given funciton returns false
TEST_CASE("Outside valid range adding string data", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);
	REQUIRE_FALSE(myDataHandler.addStringData(std::string("112")));
}

/// @brief test if the returned string is as expected
TEST_CASE("Get data as string", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);

	std::string data{};
	for (uint8_t stringIndex{0}; stringIndex < totalStrings; ++stringIndex)
		data += stringEncodedTestThree[stringIndex];
	REQUIRE(myDataHandler.addStringData(data));

	std::string readData{myDataHandler.getDataAsString()};
	REQUIRE(readData == data);
}

/// @brief test if the invertion of the data works as expected
TEST_CASE("Invert data", "[dataHandler]") {
	DataHandler myDataHandler(arraySize);

	for (uint8_t angle{0}; angle < totalAngles; ++angle)
		myDataHandler.addData(anglesTestOne[angle], angle);

	myDataHandler.invertData();

	for (int index{0}; index < arraySize; ++index) {
		std::bitset<32> y{myDataHandler.getDataArr()[index]};
		REQUIRE(y == encodedAnglesTestOneInverted[index]);
	}

	myDataHandler.invertData();

	for (int index{0}; index < arraySize; ++index) {
		std::bitset<32> y{myDataHandler.getDataArr()[index]};
		REQUIRE(y == encodedAnglesTestOne[index]);
	}
}
